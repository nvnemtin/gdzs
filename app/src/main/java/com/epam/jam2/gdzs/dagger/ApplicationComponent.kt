package com.epam.jam2.gdzs.dagger

import com.epam.jam2.gdzs.GdzsApplication
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ActivityModule::class,
    ApplicationModule::class,
    FragmentModule::class,
    ViewModelModule::class
])
interface ApplicationComponent{
    fun inject(app: GdzsApplication)
}
