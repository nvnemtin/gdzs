package com.epam.jam2.gdzs.dagger

import com.epam.jam2.gdzs.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivityInjector(): MainActivity
}
