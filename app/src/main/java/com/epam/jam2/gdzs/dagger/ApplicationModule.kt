package com.epam.jam2.gdzs.dagger

import android.content.Context
import com.epam.jam2.gdzs.GdzsApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: GdzsApplication) {
    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return application.applicationContext
    }
}
